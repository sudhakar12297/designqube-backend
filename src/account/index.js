const express = require("express");
const app = express();
const router = express.Router();
const bodyParser = require("body-parser");
const userDB = require("../../db/user");
const { success } = require("../../utils/helper");

app.use(bodyParser.json());

router.post("/register", async (req, res, next) => {
  try {
    const { email } = req.body;
    const isUserExixts = await userDB.find({ email });
    if (isUserExixts.length) {
      next({ status: 409, message: "data already exists" });
    }
    const todo = await userDB.create(req.body);
    return success(res, todo);
  } catch (err) {
    next({ status: 400, message: "failed" });
  }
});

router.post("/login", async (req, res, next) => {
  try {
    const { email, password } = req.body;
    console.log(req.body);
    const isUserExixts = await userDB.find({ email, password });
    if (isUserExixts.length === 0) {
      next({ status: 401, message: "invalid creadentials" });
    }
    return success(res, isUserExixts[0])
  } catch (err) {
    next({ status: 400, message: "failed" });
  }
});

router.use((err, req, res, next) => {
  return res.status(err.status || 400).json({
    status: err.status || 400,
    message: err.message || "something went wrong",
  });
});

module.exports = router;
