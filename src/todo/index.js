const express = require("express");
const app = express();
const router = express.Router();
const bodyParser = require("body-parser");
const todoDB = require("../../db/todo");
const { success } = require("../../utils/helper");

app.use(bodyParser.json());

router.get("/", async (req, res, next) => {
  try {
    const todos = await todoDB.find({});
    return success(res, todos);
  } catch (err) {
    next({ status: 400, message: "failed" });
  }
});

router.post("/", async (req, res, next) => {
  try {
    const todo = await todoDB.create(req.body);
    return success(res, 'task created');
  } catch (err) {
    next({ status: 400, message: "failed" });
  }
});

router.delete("/:id", async (req, res, next) => {
  try {
    await todoDB.findByIdAndRemove(req.params.id);
    return success(res, "task deleted!");
  } catch (err) {
    next({ status: 400, message: "failed" });
  }
});

app.use((err, req, res, next) => {
  return res.status(err.status || 400).json({
    status: err.status || 400,
    message: err.message || "somehting went wrong",
  });
});

module.exports = router;
