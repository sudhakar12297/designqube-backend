const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
var cors = require("cors");

const todoDB = require("./db/todo");

app.use(bodyParser.json());
app.use(cors());

mongoose.connect("mongodb://localhost/todo", { useNewUrlParser: true });

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

app.use("/api/todo", require("./src/todo"));
app.use("/api/account", require("./src/account"));

app.listen(8080, () => {
  console.log(`listening on port 8080`);
});
