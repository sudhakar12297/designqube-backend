module.exports.success = (res, payload) => {
  return res.status(200).json(payload);
};
